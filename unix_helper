#!/bin/bash

me=$(basename "${0}")

PROJECT="boltlight-doc"
DOCKER_DIR="docker"
COMPOSE_PATH="${DOCKER_DIR}/docker-compose.yml"

_show_help() {
    echo "Usage: ${me} [target]"
    echo ""
    echo "Targets:"
    echo " - build:        build a docker image of boltlight-doc"
    echo " - down:         stop and remove boltlight-doc"
    echo " - logs:         show boltlight-doc logs"
    echo " - restart:      restart boltlight-doc"
    echo " - run:          run boltlight-doc in development mode"
    echo ""
    echo " - help:         show this message"
    echo ""
    echo "Default: help"
}

params=$*
if [ -z "${params}" ]; then
    _show_help
    exit 0
fi

build() {
    docker-compose -f ${COMPOSE_PATH} build ${PROJECT}
}

down() {
    docker rm -fv ${PROJECT}
}

help() {
    _show_help
}

logs() {
    docker logs -f ${PROJECT}
}

restart() {
    docker restart ${PROJECT}
}

run() {
    cd docker
    docker-compose up \
        -d ${PROJECT}
    cd - > /dev/null
}

deploy() {
    CI_REGISTRY="${CI_REGISTRY:-hashbeam}"
    CI_PROJECT_PATH="${CI_PROJECT_PATH:-${PROJECT}}"
    # middleman build
    docker run --rm \
        -v $(pwd)/build:/srv/app/build \
        --entrypoint bash \
        ${CI_REGISTRY}/${CI_PROJECT_PATH}:${VERSION} \
        -c 'bundle exec middleman build --clean'
    # copy output for gitlab pages to deploy (public/)
    rm -rf public/ && cp -r build public
}

# Calls the set called function with the set params passed as a single word
${called_function} ${params}
