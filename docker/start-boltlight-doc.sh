#!/usr/bin/env bash

# Sets ownership
[ -z "${MYUID}" ] || usermod -u "${MYUID}" "${B_USR}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${B_USR}"
[ -n "${MYUID}" -a -n "${MYGID}" ] && \
    echo "Setting ownership to files..." && \
    chown -R --silent "${B_USR}:${B_USR}" "${APP_DIR}"

echo "Rendering template..."
python3 render.py

echo "Command: bundle exec middleman server"
exec gosu ${B_USR} bundle exec middleman server --watcher-force-polling
