```javascript
var grpc = require('grpc');
var pb = require('./boltlight_pb');
var pb_grpc = require('./boltlight_grpc_pb');

function {{ method.name|lower }}(stub) {
    {% include 'nodejs/simple_request.html' %}
    {% include 'nodejs/simple_response.html' %}
}

function main() {
    var stub = new pb_grpc.{{ method.service }}Client(
        'boltlight:1708', grpc.credentials.createInsecure());
    {{ method.name|lower }}(stub);
}

main();
```
