## {{ messageName }}
{{ message.description }}
{% if message.params | length == 0 %}
This message has no parameters.
{% else %}
Parameter | Type | Description
--------- | ---- | ----------- {% for param in message.params %}{% set pt = param.param_type.proto.split() %}
<code>{{ param.name.proto }}</code> | <code>{% if param.link is defined %}{% if pt|length > 1 %}{{ pt[0] }} [{{ pt[1] }}](#{{ param.link }}){% else %}[{{ pt[0] }}](#{{ param.link }}){% endif %}{% else %}{{ param.param_type.proto }}{% endif %}</code> | {{ param.description }} {% endfor %} {% endif %}
