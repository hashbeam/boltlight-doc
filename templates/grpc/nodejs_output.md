```javascript
{ {% for param in method.responseMessage.params %}
    "{{ param.name.javascript }}": <{{ param.param_type.javascript }}>,{% endfor %}
}
```
