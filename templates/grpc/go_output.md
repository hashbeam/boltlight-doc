```go
{ {% for param in method.responseMessage.params %}
    "{{ param.name.go }}": <{{ param.param_type.go }}>,{% endfor %}
}
```
