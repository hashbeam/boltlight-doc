```go
package main

import (
    "fmt"
    "golang.org/x/net/context"
    "google.golang.org/grpc"
    pb "boltlight"
)

func {{ method.name|lower }}(stub pb.LightningClient) {
    {% include 'go/simple_request.html' %}
    {% include 'go/simple_response.html' %}
}

func main() {
    conn, err := grpc.Dial("boltlight:1708", grpc.WithInsecure())
    if err != nil {
        fmt.Printf("Did not connect: %s", err)
    }
    defer conn.Close()
    stub := pb.New{{ method.service }}Client(conn)
    {{ method.name|lower }}(stub)
}
```
