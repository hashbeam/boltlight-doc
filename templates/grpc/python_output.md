```python
{ {% for param in method.responseMessage.params %}
    "{{ param.name.proto }}": <{{ param.param_type.python }}>,{% endfor %}
}
```
