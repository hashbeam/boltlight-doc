```python
import grpc
import boltlight_pb2 as pb, boltlight_pb2_grpc as pb_grpc

def {{ method.name|lower }}(stub):
    {% include 'python/simple_request.html' %}
    try:
        {% include 'python/simple_response.html' %}
    except grpc.RpcError as err:
        print('Error raised: {}'.format(err))

if __name__ == '__main__':
    with grpc.insecure_channel('boltlight:1708') as channel:
        stub = pb_grpc.{{ method.service }}Stub(channel)
        {{ method.name|lower }}(stub)
```
