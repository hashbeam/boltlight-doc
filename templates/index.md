---
title: Boltlight API Reference

language_tabs:
  - python: <div class='cont_languages'><img src='./images/python.png' class='language_logo'></div>
  - go: <div class='cont_languages'><img src='./images/go.png' class='language_logo'></div>
  - javascript: <div class='cont_languages'><img src='./images/nodejs.png' class='language_logo'></div>
  - shell: <div class='cont_languages'><img src='./images/bash.png' class='language_logo'></div>

toc_footers:
  - <a href='https://gitlab.com/hashbeam/boltlight'>boltlight</a>
  - <a href='mailto:hashbeam@protonmail.com'>Contact Us</a>
  - <a href='https://github.com/lord/slate'>Powered by Slate</a>

includes:
  - security
  - timeout
  - contributing

search: true

code_clipboard: true
---

# Introduction

Welcome to the gRPC API reference documentation for boltlight, a Lightning
Network node wrapper.

Boltlight APIs allow you to send commands to your LN node, whatever
implementation BOLT-compliant you have.

Boltlight currently wraps
[c-lightning](https://github.com/ElementsProject/lightning),
[eclair](https://github.com/ACINQ/eclair),
[electrum](https://github.com/spesmilo/electrum)
and [lnd](https://github.com/lightningnetwork/lnd).
We aim to make APIs available for all supported implementations, for support
details see the
[supported APIs document](https://gitlab.com/hashbeam/boltlight/blob/master/doc/supported_apis.md).

We have code examples in Python, Go, Node.js and in Bash!
Switch to your programming language of choice with the dedicated selector.

The purpose of the examples is to provide a minimal piece of code you can copy
and run.
They assume that there is a boltlight instance running on a host
reachable by using the name `boltlight` and listening for gRPC connections on
port `1708` in plaintext and without macaroon authorization.

When using on bitcoin mainnet it's highly recommended to use boltlight's
security connection over TLSv1.2, which encrypts all the data exchanged between
the client and the boltlight gRPC server, and do not disable macaroon
authorization on boltlight's configuration.
Check the [connection security](#connection-security) section for more info and
example code.

Finally, see the [connection timeout](#connection-timeout) section for info
on how to handle timeouts for gRPC connections.

<aside class="notice">
This API documentation page was generated from the boltlight's proto file,
available <a href="https://gitlab.com/hashbeam/boltlight/blob/master/boltlight/boltlight.proto">here</a>
and created with <a href="https://github.com/lord/slate">Slate</a>.
For more information about how boltlight works, see its
<a href="https://gitlab.com/hashbeam/boltlight">project page</a>.
</aside>

{% for service in services %}
# Service {{ service.name }}

{% for method in service.methods %}
## {{ method.name }}

{% if not method.streamingRequest and not method.streamingResponse %}
### Unary RPC
{% elif not method.streamingRequest and method.streamingResponse %}

{% endif %}

{{ method.description }}

{% include 'grpc/python_input.md' %}
{% include 'grpc/go_input.md' %}
{% include 'grpc/nodejs_input.md' %}
{% include 'grpc/bash_input.md' %}

> OUTPUT

{% include 'grpc/python_output.md' %}
{% include 'grpc/go_output.md' %}
{% include 'grpc/nodejs_output.md' %}
{% include 'grpc/bash_output.md' %}

{% include 'grpc/request.md' %}
{% include 'grpc/response.md' %}
{% endfor %}

{% endfor %}

# Messages
{% for messageName, message in messages.items() %}
{% include 'grpc/message.md' %}
{% endfor %}

# Enumerations
{% for enumName, enum in enums.items() %}
{% include 'grpc/enum.md' %}
{% endfor %}

# Errors
Boltlight uses the following error codes:

Error      | Status code | User message
---------- | ---------- | --------{% for errorName, error in errors.items() %}
{% include 'grpc/error.md' %}{% endfor %}
