# Connection security

You can secure the connection over TLSv1.2 and attach metadata-based
credentials (a macaroon) to authorize requests.
For more details, read boltlight's
[security page](https://gitlab.com/hashbeam/boltlight/blob/master/doc/security.md)
and gRPC documentation about
[authentication](https://grpc.io/docs/guides/auth.html).

Tip: to create server key and certificate for boltlight, run:<br>
`$ openssl req -newkey rsa:2048 -nodes -keyout server.key -x509 -days 365 -out server.crt -subj "/CN=node.example.com" -extensions SAN -config <(cat /etc/ssl/openssl.cnf <(printf "\n[SAN]\nsubjectAltName=DNS:node.example.com,DNS:boltlight,DNS:localhost,IP:127.0.0.1,IP:::1"))`

See `server_key` and `server_crt` variables in boltlight's
[configuring instructions](https://gitlab.com/hashbeam/boltlight/blob/master/doc/configuring.md).

```python
import codecs
import grpc
import boltlight_pb2_grpc as pb_grpc
import os

os.environ['GRPC_SSL_CIPHER_SUITES'] = (
    'HIGH+ECDSA:'
    'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384')

HOST = 'boltlight:1708'
CERT = '/path/to/server.crt'
MACAROONS = True
MAC = '/path/to/macaroon'

with open(CERT, 'rb') as file:
    cert = file.read()
creds = grpc.ssl_channel_credentials(root_certificates=cert)
if MACAROONS:
    auth_creds = grpc.metadata_call_credentials(metadata_callback)
    creds = grpc.composite_channel_credentials(creds, auth_creds)
with grpc.secure_channel(HOST, creds) as channel:
    stub = pb_grpc.LightningStub(channel)

def metadata_callback(context, callback):
    with open(MAC, 'rb') as file:
        macaroon_bytes = file.read()
        macaroon = codecs.encode(macaroon_bytes, 'hex')
    callback([('macaroon', macaroon)], None)
```

```go
import (
    "encoding/hex"
    "fmt"
    "golang.org/x/net/context"
    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials"
    "io/ioutil"
    pb "boltlight"
)

var HOST = "boltlight:1708"
var CERT = "/path/to/server.crt"
var MACAROONS = true
var MAC = "/path/to/macaroon"

type MacaroonCredential struct {
	macaroonBytes []byte
}

func (mac MacaroonCredential) GetRequestMetadata(ctx context.Context,
	uri ...string) (map[string]string, error) {
	md := make(map[string]string)
	md["macaroon"] = hex.EncodeToString(mac.macaroonBytes)
	return md, nil
}

func (mac MacaroonCredential) RequireTransportSecurity() bool {
	return true
}

creds, _ := credentials.NewClientTLSFromFile(CERT, "")
opts := []grpc.DialOption{
    grpc.WithTransportCredentials(creds),
}
if (MACAROONS) {
    macaroonBytes, _ := ioutil.ReadFile(MAC)
    authCreds := MacaroonCredential{}
    authCreds.macaroonBytes = macaroonBytes
    opts = append(opts, grpc.WithPerRPCCredentials(authCreds))
}
conn, err := grpc.Dial(HOST, opts...)
if err != nil {
    fmt.Printf("Did not connect: %s", err)
}
defer conn.Close()
stub := pb.NewLightningClient(conn)
```

```javascript
var fs = require('fs');
var grpc = require('grpc');
var pb_grpc = require('./boltlight_grpc_pb');

process.env.GRPC_SSL_CIPHER_SUITES = 'HIGH+ECDSA' +
                                     ':ECDHE-RSA-AES128-GCM-SHA256' +
                                     ':ECDHE-RSA-AES256-GCM-SHA384';

var HOST = 'boltlight:1708'
var CERT = '/path/to/server.crt';
var MACAROONS = true;
var MAC = '/path/to/macaroon'

var cert = fs.readFileSync(CERT);
var creds = grpc.credentials.createSsl(cert);
if (MACAROONS) {
    var macaroon_bytes = fs.readFileSync(MAC);
    var macaroon = macaroon_bytes.toString('hex');
    var metadata = new grpc.Metadata();
    metadata.add('macaroon', macaroon);
    var authCreds = grpc.credentials.createFromMetadataGenerator((_args, callback) => {
      callback(null, metadata);
    });
    var creds = grpc.credentials.combineChannelCredentials(creds, authCreds);
}
var stub = new pb_grpc.LightningClient(HOST, creds);
```
