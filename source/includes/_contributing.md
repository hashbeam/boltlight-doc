# Contributing

Contributions are very welcome!

Head to the boltlight [project page](https://gitlab.com/hashbeam/boltlight)
to open issues, feature or merge requests.

To find out more, check out boltlight's
[contributing page](https://gitlab.com/hashbeam/boltlight/blob/master/CONTRIBUTING.md).
