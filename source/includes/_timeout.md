# Connection timeout

Explicitly handling of the connection timeout is recommended.

The example code shows how to handle timeouts on gRPC channel creation
and requests.

View gRPC documentation about
[deadlines and timeouts](https://grpc.io/docs/guides/concepts.html#deadlinestimeouts).

```python
import grpc
import boltlight_pb2 as pb, boltlight_pb2_grpc as pb_grpc

host = 'boltlight:1708'
channel_timeout = 77
with grpc.insecure_channel(host) as channel:
    future_channel = grpc.channel_ready_future(channel)
    try:
        future_channel.result(timeout=channel_timeout)
    except grpc.FutureTimeoutError as err:
        # Handle gRPC channel that did not connect
        print("Failed to dial server:", err)
    else:
        request_timeout = 7
        stub = pb_grpc.LightningStub(channel)
        request = pb.GetInfoRequest()
        response = stub.GetInfo(request, timeout=request_timeout)
```

```go
import (
    "fmt"
    "golang.org/x/net/context"
    "google.golang.org/grpc"
    "log"
    "time"
    pb "boltlight"
)


host := "boltlight:1708"
channelTimeout := 77 * time.Second
conn, err := grpc.DialContext(
    context.Background(),
    host,
    grpc.WithInsecure(),
    context.WithTimeout(channelTimeout),
    grpc.WithBlock(),
)
if err != nil {
    // Handle gRPC channel that did not connect
    log.Fatalln("Failed to dial server:", err)
}

requestTimeout := 7 * time.Second
ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
defer cancel()
response, err := stub.GetInfo(ctx, &request)
```

```javascript
var grpc = require('grpc');
var pb = require('./boltlight_pb');
var pb_grpc = require('./boltlight_grpc_pb');

var host = 'boltlight:1708'
var stub = new pb_grpc.LightningClient(host, grpc.credentials.createInsecure());
var requestTimeout = new Date().setSeconds(new Date().getSeconds() + 7);
var request = new pb.GetInfoRequest();
stub.getInfo(request, {deadline: requestTimeout}, function(err, response) { });
```
