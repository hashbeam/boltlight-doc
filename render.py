"""Starting point for boltlight-doc Slate rendering."""

from proto_parser import parse_proto_json
from error_parser import parse_grpc_errors
from jinja2 import Environment, FileSystemLoader, select_autoescape

LANGUAGES = ['go', 'javascript', 'python']


def _render_template():
    """Render the Slate documentation for boltlight APIs.

    Given a template, a boltlight.json file and errors.py (from which an
    errors dictionary is obtained), renders full Slate documentation of
    boltlight APIs

    - template: `index.md`
    - proto JSON:  `boltlight.json`
    - errors file: `errors.py`
    """
    # load templates
    env = Environment(
        loader=FileSystemLoader('templates'),
        autoescape=select_autoescape(['html'])
    )
    template = env.get_template('index.md')
    # get elements to fill the template
    parsed_proto = parse_proto_json('boltlight.json', 'blink', LANGUAGES)
    ordered_errors = parse_grpc_errors()
    # fill the template
    rendered_docs = template.render(
        services=parsed_proto['services'],
        messages=parsed_proto['messages'],
        enums=parsed_proto['enums'],
        errors=ordered_errors)
    # write the rendered template to the source directory
    with open('source/index.html.md', 'w') as rendered_file:
        rendered_file.write(rendered_docs)


if __name__ == '__main__':
    _render_template()
