"""Module for parsing gRPC errors."""

from collections import OrderedDict
from json import loads
from re import sub

from boltlight.errors import ERRORS


def parse_grpc_errors():
    """Parses the different gRPC errors found within the errors.json file."""
    errors = ERRORS
    # Pop unexpected_error to reinsert it later
    unexpected = errors.pop('unexpected_error')
    # Order the rest of the elements alphabetically
    grpc_errors = OrderedDict(sorted(errors.items(), key=lambda t: t[0]))
    for key, act in errors.items():
        add_error(grpc_errors, key, act)
    # Add unexpected_error to the end of the dictionary
    add_error(grpc_errors, 'unexpected_error', unexpected)
    return grpc_errors


def add_error(errors, key, act):
    """Adds an error to the errors dictionary."""
    if key == 'unexpected_error':
        act['msg'] = '_original node error returned_'
    errors[key] = {
        'name': key,
        'code': act['code'],
        'msg': act['msg'],
        'link': key.lower()
    }
