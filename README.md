# Boltlight-doc

This is the Slate documentation project for
[boltlight](https://gitlab.com/hashbeam/boltlight).

## Build and run

To build and run boltlight-doc, run:
```bash
$ git submodule update --init
$ ./unix_helper build
$ ./unix_helper run
```

Service will run on http://localhost:4567.

Changes on static files will be automatically displayed by loading the page.

Changes to templates require a restart:
```bash
$ ./unix_helper restart
```

## Deploy website

To deploy boltlight-doc, use Gitlab's CI/CD.

Service will be deployed on https://hashbeam.gitlab.io/boltlight-doc
